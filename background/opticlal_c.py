import sys
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse, cv2, os

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dir", required = True, help = "name of the dir")
ap.add_argument("-q", "--query", required = True, help = "name of the query")

args = vars(ap.parse_args())

files = []
for file in os.listdir("/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/"+args["dir"]):
       files.append(file)

vec_one = []
vec_two = []
def getKey(item):
    return item[0]

for file in files:
    f=open("/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/"+args["dir"]+"/"+file,'r')
    data_lines = f.read().splitlines()
    
    p=open(args["query"])
    query_lines = p.read().splitlines()
    chunk_max = []
    chunk_max2 = []
    for i in range(len(data_lines)-len(query_lines)):
        chunk_d = []
        chunk_d2 = []
        for j in range(len(query_lines)):
            chunk_diff = abs(float(data_lines[i+j].split()[1])-float(query_lines[j].split()[1]))
            chunk_d.append(chunk_diff)
            chunk_diff2 = abs(float(data_lines[i+j].split()[0])-float(query_lines[j].split()[0]))
            chunk_d2.append(chunk_diff2)
        chunk_max.append(sum(chunk_d) / float(len(chunk_d)))
        chunk_max2.append(sum(chunk_d2) / float(len(chunk_d2)))
    vec_one.append([min(chunk_max),file])
    vec_two.append([min(chunk_max2),file])
    print file,min(chunk_max),min(chunk_max2)
    
print sorted(vec_one, key=getKey)
print sorted(vec_two, key=getKey)
