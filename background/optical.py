from numpy import *
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse, cv2, os

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dir", required = True, help = "name of the dir")
args = vars(ap.parse_args())



files = []
for file in os.listdir("/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/"+args["dir"]):
    if file.endswith(".rgb"):
        files.append(file)
        
a = np.fromfile('/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/'+args['dir']+'/'+files[0],dtype=np.uint8)
ind = 0
height = 288
width = 352
r=[]
g=[]
b=[]
for y in range(height):
    for x in range(width):       
        r.append(a[ind])
        g.append(a[ind+height*width])
        b.append(a[ind+height*width*2])
        ind = ind+1
big = []
q = []
for i in range(1,len(r)):
    if i%352==0:
        big.append(q)
        q=[]
    else:
        p = [b[i],g[i],r[i]]
        q.append(p)
        
image = np.array(big)
prvs = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
hsv = np.zeros_like(image)
hsv[...,1] = 255

for file in files[1:]:
    a = np.fromfile('/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/'+args['dir']+'/'+file,dtype=np.uint8)
    ind = 0
    height = 288
    width = 352
    r=[]
    g=[]
    b=[]
    for y in range(height):
        for x in range(width):       
            r.append(a[ind])
            g.append(a[ind+height*width])
            b.append(a[ind+height*width*2])
            ind = ind+1
    big = []
    q = []
    for i in range(1,len(r)):
        if i%352==0:
            big.append(q)
            q=[]
        else:
            p = [b[i],g[i],r[i]]
            q.append(p)
            
    image2 = np.array(big)
    #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    next = cv2.cvtColor(image2,cv2.COLOR_BGR2GRAY)

    flow = cv2.calcOpticalFlowFarneback(prvs,next,0.5,1,3,15,3,5,1)
    #flow = cv2.calcOpticalFlowFarneback(prvs,next, None, 0.5, 3, 15, 3, 5, 1.2, 0)

    mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
    rgb = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)

    cv2.imshow('frame2',rgb)
    print np.mean(rgb)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    elif k == ord('s'):
        cv2.imwrite('opticalfb.png',image)
        cv2.imwrite('opticalhsv.png',rgb)
    prvs = next

cv2.destroyAllWindows()
