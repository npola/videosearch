import sys
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse, cv2, os

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dir", required = True, help = "name of the dir")
ap.add_argument("-q", "--query", required = True, help = "name of the query")

args = vars(ap.parse_args())

def getKey(item):
    return item[0]

files = []
for file in os.listdir("/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/"+args["dir"]):
       files.append(file)

vec_one = []
for file in files:
    f=open("/home/akshay/Desktop/576_Pro/CSCI576_Project_Database/"+args["dir"]+"/"+file,'r')
    data_lines = f.read().splitlines()
    p=open(args["query"])
    query_lines = p.read().splitlines()
    chunk_max = []
    for i in range(len(data_lines)-len(query_lines)):
        chunk_d = []
        for j in range(len(query_lines)):
            chunk_diff = abs(float(data_lines[i+j])-float(query_lines[j]))
            chunk_d.append(chunk_diff)
        chunk_max.append(sum(chunk_d) / float(len(chunk_d)))
    vec_one.append([min(chunk_max),file])    

print sorted(vec_one, key=getKey)
